<div align="center">

<img src="./assets/RnOS_Polygon_Final.png" width="150" height="150">

# ISO Profile for RnOS

*This repository contains the profile for creating RnOS (Custom Arch-Linux ISO, with Qtile as the WM).*

</div>

<br>

**Instructions for Building the ISO**,

```
# Change directory into your preferred name for the ISO profile
$ cd rnos

# Clone the ISO Profile
$ git clone git@gitlab.com:ruturajn/RnOS_ISO_Profile.git

# Clone the RnOS_Extras repo, to get the packages, from the custom repo.
$ git clone git@gitlab.com:ruturajn/RnOS_Extras.git

# Change directory to the ISO profile, and replace the path to the custom repo
$ cd ../RnOS_ISO_Profile
$ sed -i "s|Server = file://.*|Server = file://$(find ~ -name "rnosiso_packages" 2>/dev/null)/x86_64|" pacman.conf

# Now create the output directory, and build the ISO as root
(root)$ cd ../
(root)$ mkdir rnos_iso_build
(root)$ mkarchiso -v -o ./rnos_iso_build/ ./rnos/
```

The ISO, will be available in `rnos_iso_build` and the working directory created will be `work`. Also, if you are interested
in understanding the whole process of creating a custom Arch-Linux based ISO, please [check this out](https://gitlab.com/ruturajn/RnOS_Extras/-/blob/main/RnOS_ISO_Docs/ISO_Procedure.md).
