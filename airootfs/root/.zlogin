# fix for screen readers
if grep -Fq 'accessibility=' /proc/cmdline &> /dev/null; then
    setopt SINGLE_LINE_ZLE
fi

# Set Environment variable for qt5
export QT_QPA_PLATFORMTHEME="qt5ct"

~/.automated_script.sh
