#!/bin/bash

# Author : Ruturajn <nanotiruturaj@gmail.com>

# This script runs as a part of a systemd unit. It changes the picom installation, based on
# whether RnOS is running, on physical hardware or on a VM.
# Note: The script assumes that, if RnOS is running in a VM, it will be connected to the internet.

# Get the name for the current user
user_name=$(cat /etc/passwd | grep "/home" | cut -d : -f 1)

# Tweak `picom` installation.
if [[ $(systemd-detect-virt) != "none" ]]; then
	pacman -Sy 2>/dev/null
	pacman -Rns picom-jonaburg-git --noconfirm
	pacman -S picom --noconfirm --disable-download-timeout
	sed -i 's/size\: 10/size\: 14/' /home/"${user_name}"/.config/alacritty/alacritty.yml
	cp /home/"${user_name}"/.config/picom/picom.conf /home/"${user_name}"/.config/picom/gen_picom.conf
	cp /home/"${user_name}"/.config/picom/picom_generic.conf /home/"${user_name}"/.config/picom/picom.conf
else
	sed 's|^picom.*|picom \&|' /home/"${user_name}"/.config/qtile/autostart.sh
fi

# Tweak the `sddm` theme file to fix the username capitalization issue
sed -i 's|font.capitalization: config.AllowBadUsernames == "false" ? Font.Capitalize : Font.MixedCase|// font.capitalization: config.AllowBadUsernames == "false" ? Font.Capitalize : Font.MixedCase|' /usr/share/sddm/themes/Sugar-Candy/Components/Input.qml
